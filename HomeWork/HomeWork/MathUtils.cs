﻿using System;

namespace Homework
{
    public class MathUtils
    {
        /// cambiar esto para que refleje lo que hicimos en clase.
        //public static decimal RaizCuadrada(decimal numero)
        //{
            public static int RaizCuadrada(int num)
        {
            if (0 == num) { return 0; }  // evitar dividir por cero  
            int n = (num / 2) + 1;       // estimacion incial  
            int p = (n + (num / n)) / 2;
            while (p < n)
            {
                n = p;
                p = (n + (num / n)) / 2;
            } // end while  
            return n;
        //} // end Sqrt()
    }
    }
}
