﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPSnippets.Calculadora
{
    public class CalcBase : ICalculator
    {
        int result = 0;
        int number1;
        int number2;

        public string Add(string a, string b)
        {

            number1 = Convert.ToInt32(a, 2);
            number2 = Convert.ToInt32(b, 2);
            result = number1 + number2;

            return Convert.ToString(result, 2);
        }

        public string Sub(string a, string b)
        {
            number1 = Convert.ToInt32(a, 2);
            number2 = Convert.ToInt32(b, 2);
            result = number1 - number2;

            return Convert.ToString(result, 2);
        }
        public string Multi(string a, string b)
        {
            number1 = Convert.ToInt32(a, 2);
            number2 = Convert.ToInt32(b, 2);
            result = number1 * number2;

            return Convert.ToString(result, 2);
        }
        public string Div(string a, string b)
        {
            number1 = Convert.ToInt32(a, 2);
            number2 = Convert.ToInt32(b, 2);
            result = number1 / number2;

            return Convert.ToString(result, 2);

        }

    }
}
