﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPSnippets.Calculadora
{
    public interface ICalculator
    {
        string Add(string x, string y);
        string Sub(string x, string y);
        string Multi(string x, string y);
        string Div(string x, string y);

    }


}
