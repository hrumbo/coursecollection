﻿namespace Calculator.Implementation
{
    public class ForImplementation : BaseImplementation
    {
        protected override void DoAggregation()
        {
            for (var number = 0; number != -number; number++)

            {
                this.Total += number;

            }
        }
    }
}
