﻿namespace Kata.FizzBuzz.Implementation
{
    using System;
    using System.Collections.Generic;

    public class FizzBuzzTransformer : IFizzBuzzTransformer
    {
        public IEnumerable<string> Transform(IEnumerable<int> sequence)
        {
            var response = new List<string>();



            foreach (var number in sequence)
            {
                if (!IsSequential)
                {

                }
                if (number < 0)
                {
                    throw new ArgumentException("Numero invalido.");
                }
                if (number % 5 == 0 & number % 3 == 0)
                {
                    response.Add("FizzBuzz");
                }

                else if (number % 3 == 0)
                {
                    response.Add("Fizz");
                }

                else if (number % 5 == 0)
                {
                    response.Add("Buzz");
                }

                else
                {
                    number.ToString();
                    response.Add(number.ToString());
                }

            }

            return response;

        }


        public bool IsSequential(IEnumerable<int> sequence)
        {
            var result = true;
            using (var sequenceEnum = sequence.GetEnumerator())
            {
                
                var beforeValue = null;
                while (sequenceEnum.MoveNext()&& result)
                {

                    beforeValue = sequenceEnum.Current;
                   
                    if () 
                    {
                        if (!(beforeValue == (sequenceEnum.Current -  1)))
                        {
                            result = false;
                        }
                    }
                }
                return result;
            }
        }

    }
}